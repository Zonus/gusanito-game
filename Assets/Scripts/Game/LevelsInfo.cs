﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelsInfo : MonoBehaviour
{
	//LEVEL SYNTAX
	//"SizeX.SizeY.LevelType.Tutorial.LevelElements.LevelObjectives.World"
	//	LevelElements = Exact position of tiles
	//	LevelObjectives = Objective1:Objective2:Objective3:etc... you can skip this part

	public static LevelsInfo instance;
	public static List<string> Levels;
	public static List<string> BonusLevels;
	public static int levelType;

	public int Level = 2;
	public int Bonus = 1;

	void Awake()
	{
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			DestroyImmediate(this.gameObject);
		}
	}

	void Start()
	{
		Levels = new List<string>();
		BonusLevels = new List<string>();

		/*  0*/Levels.Add("X.Y.Type.Tutorial.Elements.Objective1:Objective2.World");
		/*  1*/Levels.Add("6.6.0.1.111111 111111 111111 111111 111111 111111.Points-3000.0");
		/*  2*/Levels.Add("6.6.0.0.111111 111111 111111 111111 111111 111111.Points-100000.0");
		/*  3*/Levels.Add("6.6.0.0.102012 021432 021023 014202 410321 021314.Points-9000.0");
		/*  4*/Levels.Add("6.6.0.0.111223 321321 123123 412412 012341 012402.Points-12000.0");
		/*  5*/Levels.Add("6.6.0.0.032103 210321 032103 210321 032102 322021.Points-15000.0");
		/*  6*/Levels.Add("6.6.0.0.423423 123123 124124 120120 023023 320320.Points-18000.0");
		/*  7*/Levels.Add("6.6.0.0.000011 112222 333344 440000 111122 223333.Points-21000.0");
		/*  8*/Levels.Add("6.6.0.0.123432 321213 324120 021342 014201 213420.Points-22000.0");
		/*  9*/Levels.Add("6.6.0.0.142302 120321 412032 114414 000023 214233.Points-23000.0");
		/* 10*/Levels.Add("6.6.0.0.230214 220324 202141 111444 222232 010101.Points-24000.0");

		/* B0*/BonusLevels.Add("X.Y.Type.Tutorial.Elements.Objective1:Objective2.World");
		/* B1*/BonusLevels.Add("6.6.1.0.230214 220324 202141 111444 222232 010101.Types-10-10-10-10-10.0");
	}
}
/* TYPES
0 = Points
1 = Bonus 1
 */

/* WORLDS
0 = Beach
 */
