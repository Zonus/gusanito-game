﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellsArea : MonoBehaviour
{	
	public static CellsArea instance;

	public GameObject cellPrefab;
	public static float spacing;
	int scoreFactor = 20;
	int toggle = 0;
	public bool ended = false;

	//Level Stuff
	int sizeX;
	int sizeY;
	int type;
	int tutorial;
	int worldOffset;
	List<int> objectiveList;

	public GameObject EndedLevel;
	public GameObject PauseMenu;
	public List<Cell> currentChain;
	private List<Cell> bombs;
	public Cell[,] cells;

	LineRenderer lr;
	public TextMesh score;
	public GameObject littleScore;
	public GameObject bigScore;

	//Objectives
	float totalscore = 0;
	List<int> typesDestroyed;

	//Extras
	bool chainEmpty = false;
	int bonus = 0;
	bool cellsReady = false;
	ElementType bonusType;
	public int shape = 0;
	int shapeType = 0;
	int[,] shapeArray;
	bool bombsDeployment = false;
	
	public Sprite[] cellTypes;

	void Awake()
	{
		instance = this;

		currentChain = new List<Cell>();
		objectiveList = new List<int>();
		typesDestroyed = new List<int>();
		bombs = new List<Cell>();

		lr = GetComponent<LineRenderer>();
	}
	
	void Start()
	{
		//Fill
		if(LevelsInfo.levelType == 0)
			FillCells(LevelsInfo.Levels[LevelsInfo.instance.Level]);
		else
			FillCells(LevelsInfo.BonusLevels[LevelsInfo.instance.Bonus]);

		//Check
		if(LevelsInfo.instance.Level == 1)
			PlayerPrefs.SetInt("Played",1);
		if(tutorial > 0)
			TutorialManager.instance.ActivateTutorial(tutorial);
	}

	void LateUpdate()
	{
		EndLevel();
		//Other routines
		Bonuses();
		if(chainEmpty)
			CheckNewCells();
		if(bombsDeployment)
			GenerateBombs();

		if(Input.GetKeyUp(KeyCode.F12))
			Application.LoadLevel(Application.loadedLevelName);
	}

	void GenerateBombs()
	{
		bombsDeployment = false;
		//Trigger para cambiar animacion
		foreach(Cell c in bombs)
		{
			c.element.SetType(0,6);
		}
	}

	void CheckNewCells()
	{
		foreach(Cell c in cells)
		{
			if(!c.element.ready)
				return;
		}
		cellsReady = true;
	}

	public void Bonuses()
	{
		if(chainEmpty && cellsReady)
		{
			int tempScore = 0;
			switch(bonus)
			{
			case 2: 
				int i = 0;
				foreach(Cell c in cells)
				{
					if(c.element.elementType == bonusType)
					{
						c.DestroyElement();
						tempScore += scoreFactor * (i + 1);
						i++;
					}
				}
				ShowBigScore(tempScore);
				totalscore += tempScore;
				score.text = totalscore.ToString();
				break;
			case 3: 
				int j = 0;
				foreach(Cell c in cells)
				{
					c.DestroyElement();
					tempScore += scoreFactor * (j + 1);
					j++;
				}
				ShowBigScore(tempScore);
				totalscore += tempScore;
				score.text = totalscore.ToString();
				break;
			}
			bonus = 0;
			cellsReady = false;
			chainEmpty = false;
			OrganizeElements ();
			GenerateNewElements();
		}
	}
		
	void EndLevel()
	{
		if(!ended)
		{
			switch(type)
			{
			case 0:	
				if(totalscore >= objectiveList[0])
				{
					EndMenu();
					ended = true;
				}
				break;
			case 1: 
				for(int i = 0; i < objectiveList.Count; i++)
					if(typesDestroyed[i] < objectiveList[i])
						return;
				EndMenu();
				ended = true;
				break;
			}
		}
	}

	public void EndMenu()
	{
		EndedLevel.GetComponent<Fade>().Type("In");
		foreach(Transform t in EndedLevel.transform)
		{
			if(t.gameObject.name != "BackButton")
				t.gameObject.GetComponent<Fade>().Type("In");
			else
				t.gameObject.GetComponent<SpriteRenderer>().color -= new Color(0,0,0,1);
		}
		foreach(Cell c in cells)
			c.collider2D.enabled = false;

	}

	void FillCells(string lvl)
	{
		sizeX	 = int.Parse(lvl.Split('.')[0]);
		sizeY 	 = int.Parse(lvl.Split('.')[1]);
		type	 = int.Parse(lvl.Split('.')[2]);
		tutorial = int.Parse(lvl.Split('.')[3]);
		string levelStructure 	= lvl.Split('.')[4].Replace(" ","");
		string objectives 		= lvl.Split('.')[5];
		worldOffset	 = int.Parse(lvl.Split('.')[6]);

		ChooseObjectives(type,objectives);

		float cellSize = 0;
		cells = new Cell[sizeX,sizeY];
		shapeArray = new int[sizeX,sizeY];

		//CalculateSize
		Cell temp = ((GameObject)Instantiate(cellPrefab)).GetComponent<Cell>();
		temp.gameObject.GetComponent<SpriteRenderer>().sprite = cellTypes[0];
		cellSize = temp.gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
		Destroy(temp.gameObject);

		for(int x=0 ; x < sizeX ; x++)
		{
			Toggle ();
			for(int y=0 ; y < sizeY ; y++)
			{
				Cell cell = ((GameObject)Instantiate(cellPrefab)).GetComponent<Cell>();

				SelectSprite(Toggle(), x, y, cell);
				//cell.gameObject.GetComponent<SpriteRenderer>().sprite = cellTypes[Toggle()];

				cell.gameObject.transform.position = new Vector3((x-sizeX/2)*cellSize, (y-sizeY/2)*-cellSize -.2f, 0);
				if(sizeX % 2 == 0)
				{
					cell.gameObject.transform.position += new Vector3(cellSize/2,0,0);
				}
				cell.gameObject.transform.parent = this.gameObject.transform;
				cell.gameObject.name = "Cell " + x.ToString() + "-" + y.ToString();
				cell.coordinates = new Vector2(x,y);
				cell.CreateElement(levelStructure[x+y*sizeX], cell.gameObject.name.Substring(4), worldOffset);
				cells[x,y] = cell;
			}
		}
		this.gameObject.transform.position -= new Vector3(0,0,0.1f);
	}

	void ChooseObjectives(int type, string root)
	{
		switch(type)
		{
		case 0:
			objectiveList.Add(int.Parse(root.Split('-')[1]));
			break;
		case 1:
			for(int i = 1; i < root.Split('-').Length; i++)
			{
				objectiveList.Add(int.Parse(root.Split('-')[i]));
				typesDestroyed.Add(0);
			}
			break;
		}
	}

	void SelectSprite(int toggle, int x, int y, Cell cell)
	{
		cell.gameObject.GetComponent<SpriteRenderer>().sprite = cellTypes[toggle];

		//Corners
		if(x == 0 && y == 0 || x == 0 && y == sizeY-1 || x == sizeX-1 && y == 0 || x == sizeX-1 && y == sizeY-1)
			cell.gameObject.GetComponent<SpriteRenderer>().sprite = cellTypes[toggle + 2];
		if(x == 0 && y == 0)
		{
			return;
		}
		else if(x == 0 && y == sizeY-1)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,90));
			return;
		}
		else if(x == sizeX-1 && y == 0)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,270));
			return;
		}
		else if(x == sizeX-1 && y == sizeY-1)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,180));
			return;
		}

		//Borders
		if(x == 0 || x == sizeX-1 || y == 0 || y == sizeY-1)
			cell.gameObject.GetComponent<SpriteRenderer>().sprite = cellTypes[toggle + 4];
		if(x == 0)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,90));
			return;
		}
		else if(y == 0)
		{
			return;
		}
		else if(y == sizeY-1)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,180));
			return;
		}
		else if(x == sizeX-1)
		{
			cell.gameObject.transform.Rotate(new Vector3(0,0,270));
			return;
		}
	}

	public void UpdateLine()
	{
		int lineAux;
		if (currentChain.Count != 1)
		{
			if(currentChain.Count >= 2)
				lr.SetVertexCount ((currentChain.Count * 2) - 2);
			else
				lr.SetVertexCount(0);
		}
		else
			lr.SetVertexCount (1);
		if(currentChain.Count != 0)
			for (int x = 0; x < currentChain.Count; x++) 
			{
				lineAux = (x * 2) - 1;
				if (x == 0)
					lr.SetPosition (0, currentChain [x].element.transform.position + new Vector3(0,0,.15f));
				else if (x == currentChain.Count - 1)
					lr.SetPosition (lineAux, currentChain [x].element.transform.position + new Vector3(0,0,.15f));
				else 
				{
					lr.SetPosition (lineAux, currentChain [x].element.transform.position + new Vector3(0,0,.15f));
					lr.SetPosition (lineAux + 1, currentChain [x].transform.position + new Vector3(0.001f,0,0));
				}
			}
	}

	public void CreateChain(Cell c)
	{
		currentChain.Add(c);
	}

	public void CheckThisCell(Cell c)
	{
		if(currentChain.Count >= 1)
		{
			if(currentChain.Count > 1 && c == currentChain[currentChain.Count-2])
			{
				RemoveFromChain(c);
			}
			else
			{
				AddToChain(c);
			}
		}
	}

	public void RemoveFromChain(Cell c)
	{
		currentChain.RemoveAt(currentChain.Count-1);
		UpdateLine();
	}

	public void AddToChain(Cell c)
	{
		//Si existe una lista
		if(currentChain.Count < 1)
			return;
			
		//Si estaba previamente en la lista
		if(IsOnList(c))
			return;

		//Si es adjacente al anterior
		if(!IsAdjacent(c))
			return;

		//Si es del mismo color
		if(!IsSameType(c))
			return;

		//Si es circular
		if(currentChain[0].name == c.name)
			shapeType = 1;

		currentChain.Add(c);
		UpdateLine();
	}

	public bool IsOnList(Cell c)
	{
		if (c == currentChain [0])
		{
			shape = 1;
			return false;
		}
		return currentChain.Contains(c);
	}

	public bool IsAdjacent(Cell c)
	{
		Cell lastCellOnList = currentChain[currentChain.Count-1];
		int xDistance;
		int yDistance;
		xDistance = (int) Mathf.Abs(c.coordinates.x - lastCellOnList.coordinates.x);
		yDistance = (int) Mathf.Abs(c.coordinates.y - lastCellOnList.coordinates.y);
		
		if(xDistance <= 1 && yDistance <= 1)
			return true;
		else
			return false;
	}

	public bool IsSameType(Cell c)
	{
		if(c.element.elementType == currentChain[currentChain.Count - 1].element.elementType 		|| 	//If its the same as the one before
		   currentChain[currentChain.Count - 1].element.elementType == ElementType.World1TypeBonus 	||	//If the one before is Bonus
		   c.element.elementType == ElementType.World1TypeBonus)										//If the next one is Bonus
			return true;
		else
			return false;
	}

	public void ChainEnded()
	{
		StartCoroutine("ChainEndedCoroutine");
	}

	public IEnumerator ChainEndedCoroutine()
	{
		chainEmpty = false;
		if(currentChain.Count > 2)
		{
			//Tutorial
			if(TutorialManager.instance.tutorialNumber > 0)
				TutorialEnd();
			//Dar puntos, borrar elementos y crear nuevos
			if(type == 1)
				AddDestroyedElements();
			CreateBonusElement();
			lr.SetVertexCount(0);
			//Last Element
			if (currentChain[currentChain.Count - 1].element.bonusActive)
				bonusType = (ElementType)(Random.Range(worldOffset, (int)currentChain[currentChain.Count - 1].element.elementType));
			else
				bonusType = currentChain[currentChain.Count - 1].element.elementType;
			//Detect acceptable shape
			DetectShape();
			//Cycle
			for(int i = 0; i < currentChain.Count - shape; i++)
			{
				currentChain[i].DestroyElement();
				totalscore += scoreFactor * (i + 1);
				ShowLittleScore(scoreFactor * (i + 1), currentChain[i]);
				score.text = totalscore.ToString();
				yield return new WaitForSeconds(0.04f);
			}
			chainEmpty = true;
			shape = 0;
			OrganizeElements();
			GenerateNewElements();
		}
		currentChain.Clear();
		UpdateLine();
	}

	void DetectShape()
	{
		//La forma de rombo no puede ser mas chica que 4 y siempre es divisible entre 4
		if (currentChain.Count-1 >= 4 && (currentChain.Count-1) % 4 == 0)
		{
			int minX, maxX, minY, maxY, xSize, ySize;

			minX = maxX = (int) currentChain[0].coordinates.x;
			minY = maxY = (int) currentChain[0].coordinates.y;
			foreach(Cell c in currentChain)
			{
				if(c.coordinates.x < minX)
					minX = (int) c.coordinates.x;
				if(c.coordinates.x > maxX)
					maxX = (int) c.coordinates.x;
				if(c.coordinates.y < minY)
					minY = (int) c.coordinates.y;
				if(c.coordinates.y > maxY)
					maxY = (int) c.coordinates.y;
			}
			xSize = maxX-minX;
			ySize = maxY-minY;

			//Deben de ser de tamanio igual para ser rombo
			if(xSize != ySize)
			{
				print ("No es rombo");
				return;
			}

			//La longitud de un lado x2 debe de ser igual a la cantidad de casillas
			if((xSize * 2) != currentChain.Count-1)
			{
				print ("No es rombo");
				return;
			}

			print("Si es rombo");

			int xMid, yMid;

			xMid = (minX + (int)(xSize/2));
			yMid = (minY + (int)(ySize/2));

			for(int y = minY+1; y < maxY; y++)
			{
				int yDistanceToMidPoint = Mathf.Abs(yMid - y);
				int xCellsInThisRow = xSize - 1 - (yDistanceToMidPoint*2);
				for(int x = xMid - ((xCellsInThisRow-1)/2); x <= xMid + ((xCellsInThisRow-1)/2); x++)
				{
					//print ("x:" + x + " y:" + y + " Nombre:" + cells[x,y].name);
					bombs.Add(cells[x,y]);
				}
			}
			bombsDeployment = true;
			return;
		}
		print ("No es rombo");
		return;
	}

	void ShowLittleScore(int score, Cell cell)
	{
		GameObject temp = (GameObject)Instantiate (littleScore, new Vector3(cell.transform.position.x, cell.transform.position.y, -2), Quaternion.identity);
		temp.GetComponent<TextMesh> ().text = score.ToString ();
	}

	void ShowBigScore(int score)
	{
		GameObject temp = (GameObject)Instantiate (bigScore, new Vector3(0, 0, -2), Quaternion.identity);
		temp.GetComponent<TextMesh> ().text = score.ToString ();
	}

	public void GenerateNewElements()
	{
		foreach(Cell c in cells)
		{
			if(c.element == null)
				c.CreateRandomElement(worldOffset);
		}
	}

	public void OrganizeElements()
	{
		for(int x=0 ; x < sizeX ; x++)
		{
			for(int y = sizeY-1 ; y >= 0 ; y--)
			{
				if(cells[x,y].element == null)
				{
					for(int y2 = y-1; y2 >= 0; y2--)
					{
						if(cells[x,y2].element != null)
						{
							MoveElement(cells[x,y2], cells[x,y]);
							break;
						}
					}
				}
			}
		}
	}

	public void MoveElement(Cell from, Cell to)
	{
		to.SetElement(from.element);
		from.element = null;
	}

	/*public void CreateBonusElement()
	{
		StartCoroutine("CreateBonusElementCoroutine");
	}*/

	public void CreateBonusElement()
	{
		if(currentChain.Count >= 21)
		{
			bonus = 3;
			return;
		}
		if(currentChain.Count >= 14)
		{
			bonus = 2;
			return;
		}
		if(currentChain.Count >= 7)
		{
			if(shape == 0)
			{
				foreach(Cell c in currentChain)
				{
					if(c.element.bonusActive)
						return;
				}
				currentChain[currentChain.Count - 1].element.ActivateBonus() ;
				currentChain.RemoveAt(currentChain.Count - 1);
			}
		}
	}

	int Toggle()
	{
		if(toggle == 0)
			toggle = 1;
		else
			toggle = 0;

		return toggle;
	}

	void TutorialEnd()
	{
		bool tutorialEnded = false;
		switch(TutorialManager.instance.tutorialNumber)
		{
		case 1:	
			if(currentChain.Count >= 4)
				tutorialEnded = true;
			break;
		case 2:	
			if(currentChain.Count >= 5)
				tutorialEnded = true;
			break;
		}
		if(tutorialEnded)
		{
			TutorialManager.instance.DeactivateTutorials();
			foreach(Transform g in GameObject.Find("TutorialStuff").transform)
				g.gameObject.SendMessage("ActionStartOut",SendMessageOptions.DontRequireReceiver);
		}
		else
		{
			lr.SetVertexCount(0);
			currentChain.Clear();
		}
	}

	void AddDestroyedElements()
	{
		foreach(Cell c in currentChain)
		{
			if((int)c.element.elementType != objectiveList.Count)
				typesDestroyed[(int)c.element.elementType - worldOffset]++;
		}
		foreach(int t in typesDestroyed)
			print(t);
	}
}
