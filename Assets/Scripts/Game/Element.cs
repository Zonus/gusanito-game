﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Element : MonoBehaviour
{
	public Sprite[] types;
	public ElementType elementType;
	public Cell currentCell;
	public bool bonusActive = false;
	Animator anim;
	public bool tutorial;

	public bool fell = false;
	public bool destroy = false;

	float velocity = 2;
	public bool ready = false;

	void Start()
	{
		//anim = gameObject.GetComponent<Animator>();
	}

	void Update()
	{
		if(transform.position.y > currentCell.gameObject.transform.position.y) 
		{
			transform.position -= new Vector3(0, velocity * Time.deltaTime, 0);
		}
		else 
		{
			if(this.gameObject.transform.position.y <= currentCell.transform.position.y /*&& !fell*/)
			{
				//fell = true;
				//anim.SetBool("Fell", true);
				ready = true;
				if(!tutorial)
					transform.position = currentCell.gameObject.transform.position + new Vector3(0,0,-0.1f);
				else
					transform.position = currentCell.gameObject.transform.position + new Vector3(0,0,-0.4f);
			}
		}

		if(destroy)
			Destroy(this.gameObject);
	}

	public void SetType(char type, int world)
	{
		world = SetOffset(world);
		this.GetComponent<SpriteRenderer>().sprite = types[int.Parse("" + type)+ world];
		elementType = (ElementType)(int.Parse("" + type) + world);
	}

	public void SetType(int type, int world)
	{
		world = SetOffset(world);
		this.GetComponent<SpriteRenderer>().sprite = types[type + world];
		elementType = (ElementType)(type + world);
	}

	public void ActivateBonus()
	{
		this.GetComponent<SpriteRenderer>().sprite = types[5];
		//anim.SetBool("Bonus",true);
		elementType = (ElementType)(5);
		tutorial = false;
		bonusActive = true;
	}

	public void Fell()
	{
		anim.SetBool("Fell", false);
	}

	public void Destroy()
	{
		if(anim.GetBool("Destroy"))
			destroy = true;
		else
			anim.SetBool("Destroy", true);
	}

	int SetOffset(int world)
	{
		switch(world)
		{
		case 1:	 world += 0; break;
		}
		return world;
	}
}