﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour {

	public Element element;
	public GameObject elementPrefab;
	public Vector2 coordinates;

	public bool bonus = false;

	public void CreateElement(char type, string name, int world)
	{
		element = ((GameObject)Instantiate(elementPrefab)).GetComponent<Element>();
		//element.gameObject.transform.position = this.transform.position + new Vector3(0,1.5f,-0.2f);
		element.gameObject.transform.position = this.transform.position;
		element.SetType(type, world);
		element.gameObject.transform.parent = GameObject.Find("Elements").transform;
		element.gameObject.name = "Element" + name;
		element.currentCell = this;
	}

	public void CreateRandomElement(int world)
	{
		element = ((GameObject)Instantiate(elementPrefab)).GetComponent<Element>();
		element.gameObject.transform.position = this.transform.position + new Vector3(0,1.5f,-0.1f);
		element.SetType(Random.Range(0,5), world);
		element.gameObject.transform.parent = GameObject.Find("Elements").transform;
		element.gameObject.name = "Element" + this.name.Substring(4);
		element.currentCell = this;
	}

	public void SetElement(Element e)
	{
		element = e;
		element.currentCell = this;
		element.gameObject.name = "Element" + this.gameObject.name.Substring(4);
	}

	public void AdjustPosition(Cell changingCell)
	{
		changingCell.element = element;
		element = null;
		changingCell.element.gameObject.transform.position = changingCell.transform.position;
	}
	
	public void MouseDown()
	{
		CellsArea.instance.CreateChain(this);
	}

	public void MouseIn()
	{
		CellsArea.instance.CheckThisCell(this);
	}

	public void DestroyElement()
	{
		//element.Destroy();
		Destroy(element.gameObject);
		element = null;
	}
}
