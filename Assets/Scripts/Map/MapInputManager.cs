﻿using UnityEngine;
using System.Collections;

public class MapInputManager : MonoBehaviour
{
	bool onMobile;
	Vector3 startPositionOfClick;
	Vector3 startWorldPositionOfDrag;
	Vector3 currentWorldPositionOfDrag;
	bool isDragging = false;
	GameObject gameObjectClicked;

	public bool dragEnable = true;
	
	MapCamera mc;
	
	void Start ()
	{
		mc = this.GetComponent<MapCamera>();
		
		#if UNITY_EDITOR
		onMobile = false;
		#elif UNITY_STANDALONE
		onMobile = false;
		#elif UNITY_WEBPLAYER
		onMobile = false;
		#else
		onMobile = true;
		#endif		
	}
	
	void Update ()
	{
		if(onMobile)
		{
			DetectTouch();
		}
		else
		{
			DetectMouse();
		}
	}
	
	void DetectMouse()
	{
		if (Input.GetMouseButtonDown(0))
		{
			gameObjectClicked = GetObjectInPosition(Input.mousePosition);
			startPositionOfClick = Input.mousePosition;

			if(gameObjectClicked != null)
			{
				MouseDown();
			}
		}
		
		if (Input.GetMouseButton(0))
		{
			if(IsDragging(Input.mousePosition))
			{
				if(dragEnable)
					Drag(Input.mousePosition);
			}
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			if(!IsDragging(Input.mousePosition))
			{
				if(gameObjectClicked != null)
				{
					ClickedObject();
				}
			}
			
			gameObjectClicked = null;
			isDragging = false;
		}
	}
	
	void DetectTouch()
	{
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			gameObjectClicked = GetObjectInPosition(Input.GetTouch(0).position);
			startPositionOfClick = Input.GetTouch(0).position;

			if(gameObjectClicked != null)
			{
				MouseDown();
			}
		}
		
		if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			if(IsDragging(Input.GetTouch(0).position))
			{
				Drag(((Vector3) Input.GetTouch(0).position));
			}
		}
		
		if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			
			if(!IsDragging(Input.GetTouch(0).position))
			{
				if(gameObjectClicked != null)
				{
					ClickedObject();
				}
			}
			gameObjectClicked = null;
			isDragging = false;
		}
	}
	
	bool IsDragging(Vector3 curScreenPoint)
	{
		if(isDragging)
		{
			return true;
		}
		else
		{
			if((startPositionOfClick - curScreenPoint).sqrMagnitude > Screen.width*0.5f)
			{
				isDragging = true;
				startPositionOfClick = curScreenPoint;

				if(gameObjectClicked != null)
				{
					MouseOut();
					gameObjectClicked = null;
				}

				DragStarted(curScreenPoint);
			}
			//print (positionOfClick + " " + curScreenPoint + " = "+(positionOfClick - curScreenPoint).sqrMagnitude+ " " + (Screen.width*0.5f));
			
			return isDragging;
		}
	}
	
	GameObject GetObjectInPosition(Vector3 curScreenPoint)
	{		
		RaycastHit2D hit;
		hit = Physics2D.Raycast(this.camera.ScreenToWorldPoint(curScreenPoint), Vector2.zero);
		if(hit.collider != null)
			return (hit.collider.gameObject);
		else
			return null;
	}

	void MouseDown()
	{
		gameObjectClicked.SendMessage("MouseDown",SendMessageOptions.DontRequireReceiver);
	}

	void ClickedObject()
	{
		gameObjectClicked.SendMessage("Clicked",SendMessageOptions.DontRequireReceiver);
	}
	
	
	void MouseOut()
	{
		gameObjectClicked.SendMessage("MouseOut",SendMessageOptions.DontRequireReceiver);
	}
	
	void DragStarted(Vector3 startDragPoint)
	{
		startDragPoint.z = this.transform.position.z * -1;
		startWorldPositionOfDrag = this.camera.ScreenToWorldPoint(startDragPoint);
		mc.DragStarted();
	}
	
	void Drag(Vector3 currentDragPoint)
	{
		currentDragPoint.z = this.transform.position.z * -1;
		currentWorldPositionOfDrag = this.camera.ScreenToWorldPoint(currentDragPoint);

		mc.Drag(currentWorldPositionOfDrag - startWorldPositionOfDrag);
	}
	
	void DragEnded()
	{
		mc.DragEnded();
	}
}
