﻿using UnityEngine;
using System.Collections;

public class MapCamera : MonoBehaviour
{
	Vector3 desiredPosition;
	float speed = 5;

	float minZoom = -5;
	float maxZoom = -10;

	Vector3 startingZoomPoint;
	Vector3 startingDragPoint;

	void Start()
	{
		desiredPosition = this.transform.position;
	}

	public void MoveTo(Vector3 newDesiredPosition)
	{
		newDesiredPosition = GetTransformedCoordinates(newDesiredPosition);
		this.desiredPosition = newDesiredPosition;
	}

	public void TeleportTo(Vector3 newDesiredPosition)
	{
		newDesiredPosition = GetTransformedCoordinates(newDesiredPosition);
		this.transform.position = this.desiredPosition = newDesiredPosition;
	}

	void Update()
	{
		desiredPosition.y = Mathf.Clamp(desiredPosition.y,0,30);
		Vector3 movement = (desiredPosition - this.transform.position) * speed * Time.deltaTime;
		this.transform.position += movement;
	}

	public void DragStarted()
	{
		startingDragPoint = this.desiredPosition;
	}

	public void Drag(Vector3 value)
	{
		this.desiredPosition = new Vector3(startingDragPoint.x, startingDragPoint.y - value.y * 5, startingDragPoint.z);
	}

	public void DragEnded()
	{

	}

	//This adjust the camera to look at the coordinates while mantaining Z and X position
	private Vector3 GetTransformedCoordinates(Vector3 coordinates)
	{
		coordinates.z = desiredPosition.z;
		return coordinates;
	}
}
