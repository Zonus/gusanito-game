﻿using UnityEngine;
using System.Collections;

public class DieInXSeconds : MonoBehaviour 
{
	public float seconds;
	float tTemp;

	void Start()
	{
		tTemp = Time.time + seconds;
	}

	void Update()
	{
		if (Time.time > tTemp)
			Destroy (this.gameObject);
	}
}
