using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	SpriteRenderer FadeSprite;
	string fadeType;
	bool start = false;

	public Color originalColor;
	Color transparentColor = new Color (1,1,1,0);

	float tInterval = 0.01f;
	float tCounter;

	public float factor;
	public GameObject[] actionObjects;

	void Start()
	{
		FadeSprite = this.gameObject.GetComponent<SpriteRenderer>();
		originalColor = FadeSprite.color;
		//Action on Objects at Start
		ActionStart();
		//Action on Objects at Start
	}

	void Awake()
	{
		tCounter = Time.time + tInterval;
	}

	void LateUpdate () 
	{
		if(!start)
			return;

		if(fadeType == "In")
		{
			if(tCounter < Time.time)
			{
				tCounter = Time.time + tInterval;
				FadeSprite.color = new Color(originalColor.r, originalColor.g, originalColor.b, FadeSprite.color.a + factor);
			}
			if(FadeSprite.color.a >= originalColor.a)
			{
				start = false;
				//Action on Objects at Fade In
				ActionIn();
				//Action on Objects at Fade In
			}
		}
		if(fadeType == "Out")
		{
			if(tCounter < Time.time)
			{
				tCounter = Time.time + tInterval;
				FadeSprite.color = new Color(originalColor.r, originalColor.g, originalColor.b,  FadeSprite.color.a - factor);
			}
			if(FadeSprite.color.a <= 0)
			{
				start = false;
				//Action on Objects at Fade Out
				ActionOut();
				//Action on Objects at Fade Out
				//this.gameObject.SetActive(false);
			}
		}
	}

	void ActionStart()
	{
		if(this.gameObject.name == "Tutorial 1")
		{
			Type("In");
			actionObjects[2].SetActive(false);
		}
		else if(this.gameObject.name == "Tutorial 2")
		{
			Type("In");
			actionObjects[0].SetActive(false);
		}
		else if(this.gameObject.name == "Menu")
		{
			this.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
			foreach(Transform t in this.gameObject.transform)
				t.GetComponent<SpriteRenderer>().color = transparentColor;
		}
		else if (this.gameObject.name == "EndedMenu")
		{
			this.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
			foreach(Transform t in this.gameObject.transform)
				t.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
		}
		else if (this.gameObject.name == "PauseMenu")
		{
			this.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
			foreach(Transform t in this.gameObject.transform)
				t.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
		}
		else if (this.gameObject.name.Contains("IntroImage"))
		{
			Type("In");
		}
		else if (this.gameObject.name == "EndedWin")
		{
			this.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
			foreach(Transform t in this.gameObject.transform)
				t.gameObject.GetComponent<SpriteRenderer>().color = transparentColor;
		}
	}

	public void ActionStartIn ()
	{
		if(this.gameObject.name == "Menu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("In");
				t.gameObject.collider2D.enabled = true;
			}
		}
		else if(this.gameObject.name == "EndedMenu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("In");
				t.gameObject.collider2D.enabled = true;
			}
		}
		else if(this.gameObject.name == "PauseMenu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("In");
				t.gameObject.collider2D.enabled = true;
			}
		}
		else if(this.gameObject.name == "EndedWin")
		{
			foreach(Transform t in this.gameObject.transform)
			{	
				t.gameObject.GetComponent<Fade>().originalColor += new Color (0,0,0,1);
				t.gameObject.GetComponent<Fade>().Type("In");
			}
		}
	}

	public void ActionStartOut()
	{
		if(this.gameObject.name == "Tutorial 1")
		{
			actionObjects[3].SetActive(false);
		}
		else if(this.gameObject.name == "Tutorial 2")
		{
			actionObjects[3].SetActive(false);
		}
		else if(this.gameObject.name == "Menu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("Out");
				t.gameObject.collider2D.enabled = false;
			}
		}
		else if(this.gameObject.name == "EndedMenu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("Out");
				t.gameObject.collider2D.enabled = false;
			}
		}
		else if(this.gameObject.name == "PauseMenu")
		{
			foreach(Transform t in this.gameObject.transform)
			{
				t.gameObject.GetComponent<Fade>().Type("Out");
				t.gameObject.collider2D.enabled = false;
			}
		}
	}

	void ActionIn()
	{
		if(this.gameObject.name == "Tutorial 1")
		{
			actionObjects[2].SetActive(true);
			actionObjects[3].SetActive(true);
		}
		else if(this.gameObject.name == "Tutorial 2")
		{
			TutorialManager.instance.ActivateTutorial(2);
			actionObjects[0].SetActive(true);
			actionObjects[3].SetActive(true);
		}
		else if(this.gameObject.name == "EndedWin" || this.gameObject.name == "WinMessage")
		{
			Type("Out");
		}
	}

	void ActionOut()
	{
		if(this.gameObject.name == "Tutorial 1")
		{
			actionObjects[2].SetActive(false);
			actionObjects[0].SetActive(true);
		}
		else if(this.gameObject.name == "Tutorial 2")
		{
			actionObjects[0].SetActive(true);
			actionObjects[1].GetComponent<Fade>().Type("Out");
			actionObjects[2].GetComponent<Fade>().Type("Out");
			TutorialManager.instance.tutorialNumber = 0;
		}
		else if(this.gameObject.name == "EndedWin")
		{
			foreach(Cell c in CellsArea.instance.cells)
				c.gameObject.collider2D.enabled = false;
			actionObjects[0].GetComponent<Fade>().Type("In");
			foreach(Transform t in actionObjects[0].transform)
				t.gameObject.GetComponent<Fade>().Type("In");
		}

	}

	public void Type(string type)
	{
		start = true;
		fadeType = type;
		if(type == "In")
		{
			FadeSprite.color = new Color(0,0,0,0);
			ActionStartIn ();
		}
		else
		{
			ActionStartOut();
		}
	}
}
