﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
	bool onMobile;
	GameObject firstClickedElement;
	public GameObject currentGameObject;
	bool isPressed = false;
	
	void Start ()
	{
		#if UNITY_EDITOR
		onMobile = false;
		#elif UNITY_STANDALONE
		onMobile = false;
		#elif UNITY_WEBPLAYER
		onMobile = false;
		#else
		onMobile = true;
		#endif		
	}
	
	void Update ()
	{
		if(onMobile)
		{
			DetectTouch();
		}
		else
		{
			DetectMouse();
		}
	}
	
	void DetectMouse()
	{
		if(!isPressed)
		{
			if (Input.GetMouseButtonDown(0))
			{
				currentGameObject = GetObjectInPosition(Input.mousePosition);
				if(currentGameObject != null)
				{
					firstClickedElement = currentGameObject;
					MouseDown();
					isPressed = true;
				}
			}
		}
		else
		{
			if (Input.GetMouseButton(0))
			{
				if(currentGameObject != GetObjectInPosition(Input.mousePosition))
				{
					currentGameObject = GetObjectInPosition(Input.mousePosition);
					if(currentGameObject != null)
						MouseIn();
					else
						MouseOut();
				}
			}
		}

		if (Input.GetMouseButtonUp(0))
		{
			MouseUp();
			currentGameObject = null;
			isPressed = false;
		}
	}
	
	void DetectTouch()
	{
		if(!isPressed)
		{
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			{
				currentGameObject = GetObjectInPosition(Input.GetTouch(0).position);
				if(currentGameObject != null)
				{
					firstClickedElement = currentGameObject;
					MouseDown();
					isPressed = true;
				}
			}
		}
		else
		{
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				if(currentGameObject != GetObjectInPosition(Input.GetTouch(0).position))
				{
					currentGameObject = GetObjectInPosition(Input.GetTouch(0).position);
					if(currentGameObject != null)
						MouseIn();
					else
						MouseOut();
				}
			}
		}
		
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			MouseUp();
			currentGameObject = null;
			isPressed = false;
		}
	}
	
	GameObject GetObjectInPosition(Vector3 curScreenPoint)
	{		
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(curScreenPoint), Vector2.zero);
		if(hit.collider != null)
			return (hit.collider.gameObject);
		else
			return null;
	}

	
	
	void MouseDown()
	{
		currentGameObject.SendMessage("MouseDown",SendMessageOptions.DontRequireReceiver);
	}
	
	void MouseOut()
	{
		firstClickedElement.SendMessage("MouseOut",SendMessageOptions.DontRequireReceiver);
	}
	
	void MouseIn()
	{
		currentGameObject.SendMessage("MouseIn",SendMessageOptions.DontRequireReceiver);
	}
	
	void MouseUp()
	{
		if(Application.loadedLevelName == "Game" || Application.loadedLevelName == "Bonus Game")
		{
			CellsArea.instance.ChainEnded();
		}
		if(currentGameObject != null)
		{
			if(currentGameObject.name.Contains("Button"))
			{
				currentGameObject.SendMessage("Clicked",SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
