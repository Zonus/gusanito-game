﻿using UnityEngine;
using System.Collections;

public class TutorialManager : MonoBehaviour 
{
	public static TutorialManager instance;

	public GameObject[] tutorialMessages;
	public int tutorialNumber = 1;

	void Awake()
	{
		instance = this;
	}

	public void ActivateTutorial(int number)
	{
		tutorialNumber = number;
		tutorialMessages[0].SetActive(true);
		tutorialMessages[tutorialNumber].SetActive(true);
		tutorialMessages[tutorialNumber+1].SetActive(true);
		switch(tutorialNumber)
		{
		case 1: 
			ActivateCell(1,2);
			ActivateCell(2,2);
			ActivateCell(3,2);
			ActivateCell(4,2);
				break;
		case 2: 
			ActivateCell(0,5);
			ActivateCell(1,4);
			ActivateCell(2,3);
			ActivateCell(3,2);
			ActivateCell(4,1);
				break;
		}
	}

	public void DeactivateTutorials()
	{
		foreach(Cell c in CellsArea.instance.cells)
		{
			c.gameObject.transform.position = new Vector3(c.gameObject.transform.position.x,c.gameObject.transform.position.y,0);
		}
		foreach(GameObject g in tutorialMessages)
		{
			if(g.activeSelf && (g.name != "BlackTransparent" && g.name != "TutorialFrame"))
				g.GetComponent<Fade>().Type("Out");
		}
	}

	void ActivateCell(int x, int y)
	{
		CellsArea.instance.cells[x,y].element.tutorial = true;
	}
}
