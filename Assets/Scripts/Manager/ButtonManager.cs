﻿using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour
{
	Color normalColor = new Color(1,1,1);
	Color pressedColor = new Color(0.5f,0.5f,0.5f);

	public GameObject[] obj;

	void MouseDown()
	{
		this.GetComponent<SpriteRenderer>().color = pressedColor;
	}
	
	void MouseOut()
	{
		this.GetComponent<SpriteRenderer>().color = normalColor;
	}
	
	void Clicked()
	{
		this.GetComponent<SpriteRenderer> ().color = normalColor;
		/*BUTTON behavior*/
		//Game Buttons
		if (this.name == "HomeButton") 
		{
			Application.LoadLevel ("Map");
		} 
		else if (this.name == "ContinueButton") 
		{
			LevelsInfo.instance.Level++;
			Application.LoadLevel ("Game");
		} 
		else if (this.name == "RestartButton") 
		{
			Application.LoadLevel ("Game");
		}
		else if(this.name == "PauseButton")
		{
			//ParchePrueba
			Camera.main.GetComponent<BlurEffect>().enabled = true;

			obj[0].GetComponent<Fade>().Type("In");
			this.gameObject.collider2D.enabled = false;
		}
		else if(this.name == "BackButton")
		{
			//ParchePrueba
			Camera.main.GetComponent<BlurEffect>().enabled = false;

			obj[0].GetComponent<Fade>().Type("Out");
			obj[1].collider2D.enabled = true;
		}
		//Intro Buttons
		else if (this.name == "MenuPlayButton") 
		{
			if (PlayerPrefs.GetInt ("Played") == 1)
				Application.LoadLevel ("Map");
			else
				Application.LoadLevel ("Game");
		}
		//Map Buttons
		else if (this.name.Contains("Level"))
		{
			obj[0].GetComponent<Fade>().Type("In");
			foreach(Transform t in obj[1].transform)
			{
				t.collider2D.enabled = false;
			}
			obj[2].GetComponent<MapInputManager>().dragEnable = false;
			if(this.name.Contains("Bonus"))
			{
				LevelsInfo.instance.Bonus = int.Parse(this.name.Substring(18));
				LevelsInfo.levelType = 1;
			}
			else
			{
				LevelsInfo.instance.Level = int.Parse(this.name.Substring(12));
				LevelsInfo.levelType = 0;
			}
		}
		else if(this.name == "CancelGameButton")
		{
			obj[0].GetComponent<Fade>().Type("Out");
			foreach(Transform t in obj[1].transform)
			{
				t.collider2D.enabled = true;
			}
			obj[2].GetComponent<MapInputManager>().dragEnable = true;
		}
		else if(this.name == "GameButton")
		{
			Application.LoadLevel("Game");
		}
		else if(this.name == "AlbumButton")
		{
			Application.LoadLevel("Album");
		}
		//Album Buttons
		else if(this.name == "ShareButton")
		{
			SPShareUtility.FacebookShare("Test");
			/*FacebookManager.instance.friendList = SPFacebook.instance.friendsList;
			foreach(FacebookUserInfo f in FacebookManager.instance.friendList)
			{
				print(f.first_name);
			}*/
		}
		/*BUTTON behavior*/
	}
}
